import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  cart: [],
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    deleteItem: (state, action) => {
      state.cart = state.cart.filter((item) => item.pizzaId !== action.payload);
    },
    addItem: (state, action) => {
      state.cart = [...state.cart, action.payload];
    },
    clearCart: (state) => {
      state.cart = [];
    },

    increaseQuantitiy: (state, action) => {
      const item = state.cart.find((item) => item.pizzaId == action.payload);
      item.quantity++;
      item.totalPrice = item.quantity * item.unitPrice;
    },
    decreaseQuantitiy: (state, action) => {
      const item = state.cart.find((item) => item.pizzaId == action.payload);
      item.quantity--;
      item.totalPrice = item.quantity * item.unitPrice;
//if 0 deleglete element from cart
if(item.quantity<1){
cartSlice.caseReducers.deleteItem(state, action)
}

    },
  },
});

export const {
  deleteItem,
  addItem,
  clearCart,
  increaseQuantitiy,
  decreaseQuantitiy,
  getTotalQuantitiy,
  getTotalPrice,
} = cartSlice.actions;

export default cartSlice.reducer;
// cart: [
//   {
//     pizzaId: 123,
//     name: "Margarita",
//     unitPrice: 12,
//     quantitiy: 2,
//     totalPrice: 40,
//   },
// ],

export function getTotalCartQty(state) {
  return state.cart.cart.reduce(
    (prev, current) => (prev += current.quantity),
    0
  );
}

export function getTotalCartPrice(state) {
  return state.cart.cart.reduce(
    (prev, current) => (prev += current.totalPrice),
    0
  );
}

export const getCurrentItemQuantity = (id) => (state) => {
  return state.cart.cart.find((res) => res.pizzaId == id)?.quantity || 0;
};
